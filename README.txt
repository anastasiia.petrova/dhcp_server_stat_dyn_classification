CREATED ON 17.04.24 BY ANASTASIIA PETROVA

The task is to classify ip addresses in the local network. 
We have two files from dhcp server it is dhcpd.conf and dhcpd.leases. And we have
zenmapOutput.txt file.

Task: dhcp server becomes the request from some host and its mac address, it needs
to choose the ip address for this host. It can happen dynamically or static.

In dhcpd.conf: if dhcp server becomes request from some host it looks up in this file
and if the mac address is in this file then it gives the host the static address from
this file

In dhcpd.leases: here dhcp server saves the hosts whom it gave dynamic ips

Although it can happen as well that on the host locally we have such settings that
it always becomes some static ip in this network. In this case dhcp server gives it
static ip.

In zenmapOutput.txt: scan of the whole network, so all ips which are there

TODO:

Iterate through all ips in zenmapOutput.txt. 
Check if this ip is in dhcpd.leases. If it is then save to results/dynamic_ips.txt and break.
If not then check if this ip is in dhcpd.conf. If it is then save all info about this ip in
results/static_ips_and_information.txt
If this ip is not in dhcpd.conf and not in dhcpd.leases then save all info about this ip in
results/static_ips_and_information.txt

END