def lines_that_start_with(string, fp):
    return [line for line in fp if line.startswith(string)]
def check_string_in_file(string, fp):
    found = False
    for line in fp:
        if string in line:
            found = True
            break
    return found


print("[!] Starting the classification")
with open("zenmapOutput.txt", "r") as fp:
    for line in lines_that_start_with("Nmap scan report for", fp):
        #delete the last characters in the line that would be ")\n"
        line = line.rstrip(line[-1])
        line = line.rstrip(line[-1])
        #get ip address
        ip_address = line.split("134.130.171",1)[1]
        ip_address = "134.130.171"+ip_address
        #get domain name
        domain_name = line.split(".itv.rwth-aachen.de",1)[0]
        domain_name = domain_name.split()[-1]
        #searching mac address
        found =  False
        mac_address = "empty string"
        #open again the same file
        with open("zenmapOutput.txt", "r") as hp:
            for someLine in hp:
                if (someLine.startswith(line)):
                    found = True
                if (found):
                    #search now for mac address, i.e. for the first line that start with mac address
                    if (someLine.startswith("MAC Address")):
                        #mac address is found so save it and break up
                        mac_address = someLine.split("Address: ", 1)[1]
                        break
        print("[+] Working with ", ip_address)
        #until this point we know all the info acout this ip: IP ADDRESS, MAC ADDRESS, DOMAIN NAME
        
        #----------------------------------------------------------------------------------
        
        #now we need to find out whether it is dynamic or static_ip_address
        #check whether it is in dhcpd.leases or not
        dynamic_or_static = "static"
        with open("dhcpd_leases.txt", "r") as leases:
            toSearch = ip_address+" {"
            if (check_string_in_file(toSearch, leases)):
                dynamic_or_static = "dynamic"
                #write to the file and break up the execution for this line
                with open('dynamic_ips.txt', 'a') as dyn:
                    dyn.write("DYNAMIC IP ADDRESS: " + ip_address + '\n')
                    dyn.write("MAC ADDRESS: " + mac_address + '\n')
                    dyn.write("DOMAIN NAME: " + domain_name + '\n')
                    dyn.write("---------------------------------------------------"  + '\n')
                    continue
        
        #----------------------------------------------------------------------------------
        
        #now we need to find out the last parameter, it is dhcp_conf_or_locally
        dhcp_conf_or_locally = "Speficied locally"
        with open("dhcpd_conf.txt", "r") as conf:
            toASearch = ip_address+";"
            if (check_string_in_file(toASearch, conf)):
                dhcp_conf_or_locally = "Speficied in dhcpd.conf"
        #write to the file 
        with open('static_ips_and_info.txt', 'a') as stat:
            stat.write("STATIC IP ADDRESS: " + ip_address + '\n')
            stat.write("MAC ADDRESS: " + mac_address + '\n')
            stat.write("DOMAIN NAME: " + domain_name + '\n')
            stat.write("IN dhcp.conf OR LOCALLY: " + dhcp_conf_or_locally + '\n')
            stat.write("---------------------------------------------------"  + '\n')
print("[!] Execution ended")